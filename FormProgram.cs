﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO.Ports;
using System.Linq;
using System.Security.Authentication.ExtendedProtection;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using FinsterTelemetryProtocol;
using System.Windows.Forms.VisualStyles;

namespace FinsterMissionControl
{
    public partial class FormProgram : Form
    {
        private SerialPort _radio;
        private RadioLinkPacketParser _parser;
        private IList<Flight> _flights;

        private Timer _connectionActivityBubbleTimer;
        private ActivityTimer _channelActivity;

        public FormProgram()
        {
            InitializeComponent();
        }

        public void Connected()
        {
            connectionStatusLabel.ForeColor = SystemColors.ControlText;
            connectionStatusLabel.Text = string.Format("Connected ({0} at {1})", _radio.PortName, _radio.BaudRate);

            _connectionActivityBubbleTimer = new Timer();
            _connectionActivityBubbleTimer.Interval = 10;
            _connectionActivityBubbleTimer.Enabled = true;
            _connectionActivityBubbleTimer.Tick += (sender, args) =>
            {
                connectionActivityBubble.Image = Images.Inactive;
            };

            MenuItem[] items = this.Menu.MenuItems.Find("radio", true);
            items[0].MenuItems[0].Enabled = false;
            items[0].MenuItems[1].Enabled = true;
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            _flights = new List<Flight>();
            for (int i = 0; i < RadioLinkPacket.MAX_CHANNEL + 1; i++)
            {
                _flights.Add(new Flight(i));
            } 

            InitializeMenu();
            InitializeToolStrip();

            InitializeRadio();

            _parser = new RadioLinkPacketParser();

            InitializeDataBinding();
        }

        private void InitializeDataBinding()
        {
            Binding b = new Binding("Text", _flights[1].CurrentFrame, "Latitude");
            b.NullValue = "N/A";
            latitude1.DataBindings.Add(b);
        }

        private void InitializeRadio()
        {
            _radio = new SerialPort();

            _radio.DataReceived += _radio_DataReceived;
        }

        private void _radio_DataReceived(object sender, SerialDataReceivedEventArgs e)
        {
            //connectionActivityBubble.Image = Images.Active;
            //_connectionActivityBubbleTimer.Start();

            if (e.EventType == SerialData.Chars)
            {
                SerialPort sp = (SerialPort) sender;

                // parse packet, byte by byte
                while (sp.BytesToRead > 0)
                {
                    byte b = (byte) sp.ReadByte();
#if DEBUG
                    //Console.Write("{0:X2} ", b);
#endif
                    if (_parser.Encode(b))
                    {
                        //Console.WriteLine();
                        byte[] raw = _parser.Packet.Data;
                        TelemetryPacket p = TelemetryPacket.GetFromRawData(raw);

                        int channel = _parser.Packet.Channel;
                        _flights[channel].Update(p);
                        _channelActivity.Update(channel);
                    }
                }
            }
        }

        private void InitializeToolStrip()
        {
            statusStrip1.Items.Add("Channel Activity:");

            List<ToolStripItem> channelActivityList = new List<ToolStripItem>();

            for (int i = 0; i < 16; i++)
            {
                ToolStripItem channel = new ToolStripStatusLabel();
                channel.ForeColor = SystemColors.ControlDark;
                channel.Text = "" + i;
                channelActivityList.Add(channel);
            }

            ToolStripItem gpsChannel = new ToolStripStatusLabel();
            gpsChannel.ForeColor = SystemColors.ControlDark;
            gpsChannel.Text = "GPS";
            channelActivityList.Add(gpsChannel);

            statusStrip1.Items.AddRange(channelActivityList.ToArray());

            _channelActivity = new ActivityTimer(_flights, channelActivityList);

            
            //channelActivityList[16].BackColor = Color.Green;
            //channelActivityList[16].ForeColor = SystemColors.ControlLightLight;
        }

        private void InitializeMenu()
        {
            this.Menu = new MainMenu();
            MenuItem item;

            item = new MenuItem("File");
            this.Menu.MenuItems.Add(item);
            item.MenuItems.Add("Save...", new EventHandler(Save_Click));
            item.MenuItems.Add("Export CSV...", new EventHandler(Export_Click));

            item = new MenuItem("Data");
            this.Menu.MenuItems.Add(item);
            item.MenuItems.Add("Download Data...", new EventHandler(Download_Click));

            item = new MenuItem("Radio");
            item.Name = "radio";
            this.Menu.MenuItems.Add(item);
            MenuItem connect = new MenuItem();
            connect.Enabled = true;
            connect.Text = @"Connect...";
            connect.Click += Connect_Click;
            item.MenuItems.Add(connect);

            MenuItem disconnect = new MenuItem();
            disconnect.Enabled = false;
            disconnect.Text = @"Disconnect";
            disconnect.Click += (sender, args) =>
            {
                if (_radio.IsOpen)
                {
                    _radio.Close();
                }

                MenuItem[] items = this.Menu.MenuItems.Find("radio", true);
                items[0].MenuItems[0].Enabled = true;
                items[0].MenuItems[1].Enabled = false;

                connectionStatusLabel.Text = string.Format("Not Connected");
            };
            item.MenuItems.Add(disconnect);

            item = new MenuItem("Help");
            this.Menu.MenuItems.Add(item);
            item.MenuItems.Add("About...", new EventHandler(About_Click));
        }

        private void About_Click(object sender, EventArgs e)
        {
            StringBuilder b = new StringBuilder();
            b.AppendLine(@"This software is built to interface with the ESS 472 TLM v5 board.");
            b.AppendLine(@"Built by Caleb Moore, April 2015.");
            MessageBox.Show(b.ToString());
        }

        private void Connect_Click(object sender, EventArgs e)
        {
            if (_radio.IsOpen)
            {
                MessageBox.Show(@"There is already an open connection.");
                return;
            }

            FormConnect connectForm = new FormConnect(_radio, this);       
            connectForm.Show();
        }

        private void Save_Click(object sender, EventArgs e)
        {
            throw new NotImplementedException();
        }

        private void Export_Click(object sender, EventArgs e)
        {
            throw new NotImplementedException();
        }

        private void Download_Click(object sender, EventArgs e)
        {
            throw new NotImplementedException();
        }
    }
}
