﻿using System;
using System.Collections.Generic;
using System.Deployment.Application;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using FinsterTelemetryProtocol;

namespace FinsterMissionControl
{
    public class ActivityTimer
    {
        private delegate void IndicatorUpdateCallback(int i);

        private ActivityIndicator[] _activityIndicators;
        
        private IList<Flight> _flights;

        public IList<ToolStripItem> Indicators { get; set; }

        public ActivityTimer(IList<Flight> flights, IList<ToolStripItem> indicators)
        {
            _activityIndicators = new ActivityIndicator[RadioLinkPacket.MAX_CHANNEL + 1];

            _flights = flights;
            Indicators = indicators;

            for (int i = 0; i < RadioLinkPacket.MAX_CHANNEL + 1; i++)
            {
                _activityIndicators[i] = new ActivityIndicator(indicators[i], flights[i]);
            }
        }

        public void Update(int channel)
        {
            _activityIndicators[channel].Update();
        }
    }

    internal class ActivityIndicator
    {
        public static readonly int ConnectionTimeout = 10 * 1000; // time before connection deemed lost
        public static readonly int ActivityBlipTimeout = 50; // flashes when data is received

        private delegate void IndicatorUpdateCallback(ToolStripItem i, Flight f);

        public System.Timers.Timer ConnectionTimer;
        public System.Timers.Timer ActivityTimer;
        public ToolStripItem Indicator;
        public Flight FlightRef;

        public ActivityIndicator(ToolStripItem indicator, Flight flight)
        {
            Indicator = indicator;
            FlightRef = flight;

            ConnectionTimer = new System.Timers.Timer(ConnectionTimeout);
            ConnectionTimer.Elapsed += (s, args) =>
            {
                ConnectionTimer.Stop();
                ConnectionClear(Indicator, FlightRef);
            };

            ActivityTimer = new System.Timers.Timer {Interval = ActivityBlipTimeout};
            ActivityTimer.Elapsed += (s, args) =>
            {
                ActivityTimer.Stop();
                ActivityClear(Indicator, FlightRef);
            };
        }

        public void Update()
        {
            Update(Indicator, FlightRef);

            ActivityTimer.Interval = ActivityBlipTimeout;
            ActivityTimer.Start();
            ConnectionTimer.Interval = ConnectionTimeout;
            ConnectionTimer.Start();
            FlightRef.Active = true;
        }

        private void Update(ToolStripItem i, Flight f)
        {
            if (i.GetCurrentParent().InvokeRequired)
            {
                IndicatorUpdateCallback d = Update;
                i.GetCurrentParent().Invoke(d, new object[] { i, f });
            }
            else
            {
                i.ForeColor = SystemColors.ControlLightLight;
                i.BackColor = Color.Green;
            }
        }

        private void ConnectionClear(ToolStripItem i, Flight f)
        {
            if (i.GetCurrentParent().InvokeRequired)
            {
                IndicatorUpdateCallback d = new IndicatorUpdateCallback(ConnectionClear);
                i.GetCurrentParent().Invoke(d, new object[] { i, f });
            }
            else
            {
                i.ForeColor = SystemColors.ControlDark;
                f.Active = false;
            }
        }

        private void ActivityClear(ToolStripItem i, Flight f)
        {
            if (i.GetCurrentParent().InvokeRequired)
            {
                IndicatorUpdateCallback d = new IndicatorUpdateCallback(ActivityClear);
                i.GetCurrentParent().Invoke(d, new object[] { i, f });
            }
            else
            {
                i.ForeColor = f.Active ? Color.Green : SystemColors.ControlDark;
                i.BackColor = Color.Transparent;
            }
        }
    }
}
