﻿using System.Windows.Forms;

namespace FinsterMissionControl
{
    partial class FormProgram
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.connectionActivityBubble = new System.Windows.Forms.ToolStripStatusLabel();
            this.connectionStatusLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this.droppedPacketsLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this.droppedPacketsValue = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripStatusLabel1 = new System.Windows.Forms.ToolStripStatusLabel();
            this.receivedPacketsValue = new System.Windows.Forms.ToolStripStatusLabel();
            this.activeConnectionsLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this.activeConnectionsValueLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this.rocketTabControl = new System.Windows.Forms.TabControl();
            this.rocket1Tab = new System.Windows.Forms.TabPage();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.metLabel1 = new System.Windows.Forms.GroupBox();
            this.label1 = new System.Windows.Forms.Label();
            this.radioLabel1 = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
            this.label44 = new System.Windows.Forms.Label();
            this.label43 = new System.Windows.Forms.Label();
            this.label42 = new System.Windows.Forms.Label();
            this.label41 = new System.Windows.Forms.Label();
            this.label48 = new System.Windows.Forms.Label();
            this.label49 = new System.Windows.Forms.Label();
            this.flightStatusLabel1 = new System.Windows.Forms.Label();
            this.stopRecordingButton1 = new System.Windows.Forms.Button();
            this.telemetryDataGroupBox1 = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel4 = new System.Windows.Forms.TableLayoutPanel();
            this.flowLayoutPanel3 = new System.Windows.Forms.FlowLayoutPanel();
            this.label16 = new System.Windows.Forms.Label();
            this.textBox6 = new System.Windows.Forms.TextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.textBox7 = new System.Windows.Forms.TextBox();
            this.label18 = new System.Windows.Forms.Label();
            this.textBox8 = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.label8 = new System.Windows.Forms.Label();
            this.latitude1 = new System.Windows.Forms.TextBox();
            this.label32 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.label33 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.textBox3 = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.flowLayoutPanel2 = new System.Windows.Forms.FlowLayoutPanel();
            this.label10 = new System.Windows.Forms.Label();
            this.textBox4 = new System.Windows.Forms.TextBox();
            this.label34 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.textBox5 = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.flowLayoutPanel4 = new System.Windows.Forms.FlowLayoutPanel();
            this.label19 = new System.Windows.Forms.Label();
            this.textBox9 = new System.Windows.Forms.TextBox();
            this.label20 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.textBox10 = new System.Windows.Forms.TextBox();
            this.label21 = new System.Windows.Forms.Label();
            this.flowLayoutPanel5 = new System.Windows.Forms.FlowLayoutPanel();
            this.label22 = new System.Windows.Forms.Label();
            this.textBox11 = new System.Windows.Forms.TextBox();
            this.label23 = new System.Windows.Forms.Label();
            this.textBox12 = new System.Windows.Forms.TextBox();
            this.label24 = new System.Windows.Forms.Label();
            this.textBox13 = new System.Windows.Forms.TextBox();
            this.label25 = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.flowLayoutPanel6 = new System.Windows.Forms.FlowLayoutPanel();
            this.label27 = new System.Windows.Forms.Label();
            this.textBox14 = new System.Windows.Forms.TextBox();
            this.label28 = new System.Windows.Forms.Label();
            this.textBox15 = new System.Windows.Forms.TextBox();
            this.label29 = new System.Windows.Forms.Label();
            this.textBox16 = new System.Windows.Forms.TextBox();
            this.label30 = new System.Windows.Forms.Label();
            this.label31 = new System.Windows.Forms.Label();
            this.label35 = new System.Windows.Forms.Label();
            this.flowLayoutPanel7 = new System.Windows.Forms.FlowLayoutPanel();
            this.label36 = new System.Windows.Forms.Label();
            this.textBox17 = new System.Windows.Forms.TextBox();
            this.label37 = new System.Windows.Forms.Label();
            this.textBox18 = new System.Windows.Forms.TextBox();
            this.label38 = new System.Windows.Forms.Label();
            this.textBox19 = new System.Windows.Forms.TextBox();
            this.label39 = new System.Windows.Forms.Label();
            this.label40 = new System.Windows.Forms.Label();
            this.eventLog1 = new System.Windows.Forms.TextBox();
            this.label50 = new System.Windows.Forms.Label();
            this.newConnectionTab = new System.Windows.Forms.TabPage();
            this.tableLayoutPanel5 = new System.Windows.Forms.TableLayoutPanel();
            this.label47 = new System.Windows.Forms.Label();
            this.label45 = new System.Windows.Forms.Label();
            this.label46 = new System.Windows.Forms.Label();
            this.textBox20 = new System.Windows.Forms.TextBox();
            this.newFlightChannelSelect = new System.Windows.Forms.NumericUpDown();
            this.flowLayoutPanel8 = new System.Windows.Forms.FlowLayoutPanel();
            this.newFlightTypeStandardRadioButton = new System.Windows.Forms.RadioButton();
            this.newFlightTypeLegacyRadioButton = new System.Windows.Forms.RadioButton();
            this.button1 = new System.Windows.Forms.Button();
            this.flightBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.statusStrip1.SuspendLayout();
            this.rocketTabControl.SuspendLayout();
            this.rocket1Tab.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            this.metLabel1.SuspendLayout();
            this.radioLabel1.SuspendLayout();
            this.tableLayoutPanel3.SuspendLayout();
            this.telemetryDataGroupBox1.SuspendLayout();
            this.tableLayoutPanel4.SuspendLayout();
            this.flowLayoutPanel3.SuspendLayout();
            this.flowLayoutPanel1.SuspendLayout();
            this.flowLayoutPanel2.SuspendLayout();
            this.flowLayoutPanel4.SuspendLayout();
            this.flowLayoutPanel5.SuspendLayout();
            this.flowLayoutPanel6.SuspendLayout();
            this.flowLayoutPanel7.SuspendLayout();
            this.newConnectionTab.SuspendLayout();
            this.tableLayoutPanel5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.newFlightChannelSelect)).BeginInit();
            this.flowLayoutPanel8.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.flightBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.connectionActivityBubble,
            this.connectionStatusLabel,
            this.droppedPacketsLabel,
            this.droppedPacketsValue,
            this.toolStripStatusLabel1,
            this.receivedPacketsValue,
            this.activeConnectionsLabel,
            this.activeConnectionsValueLabel});
            this.statusStrip1.Location = new System.Drawing.Point(0, 522);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(1087, 24);
            this.statusStrip1.TabIndex = 0;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // connectionActivityBubble
            // 
            this.connectionActivityBubble.ForeColor = System.Drawing.SystemColors.ControlDark;
            this.connectionActivityBubble.Image = global::FinsterMissionControl.Images.Inactive;
            this.connectionActivityBubble.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.connectionActivityBubble.Name = "connectionActivityBubble";
            this.connectionActivityBubble.Padding = new System.Windows.Forms.Padding(6, 0, 0, 0);
            this.connectionActivityBubble.Size = new System.Drawing.Size(18, 19);
            // 
            // connectionStatusLabel
            // 
            this.connectionStatusLabel.BorderSides = System.Windows.Forms.ToolStripStatusLabelBorderSides.Right;
            this.connectionStatusLabel.BorderStyle = System.Windows.Forms.Border3DStyle.Etched;
            this.connectionStatusLabel.ForeColor = System.Drawing.SystemColors.ControlText;
            this.connectionStatusLabel.Name = "connectionStatusLabel";
            this.connectionStatusLabel.Size = new System.Drawing.Size(92, 19);
            this.connectionStatusLabel.Text = "Not Connected";
            // 
            // droppedPacketsLabel
            // 
            this.droppedPacketsLabel.Name = "droppedPacketsLabel";
            this.droppedPacketsLabel.Size = new System.Drawing.Size(99, 19);
            this.droppedPacketsLabel.Text = "Dropped Packets:";
            // 
            // droppedPacketsValue
            // 
            this.droppedPacketsValue.BorderSides = System.Windows.Forms.ToolStripStatusLabelBorderSides.Right;
            this.droppedPacketsValue.BorderStyle = System.Windows.Forms.Border3DStyle.Etched;
            this.droppedPacketsValue.Name = "droppedPacketsValue";
            this.droppedPacketsValue.Size = new System.Drawing.Size(17, 19);
            this.droppedPacketsValue.Text = "0";
            // 
            // toolStripStatusLabel1
            // 
            this.toolStripStatusLabel1.Name = "toolStripStatusLabel1";
            this.toolStripStatusLabel1.Size = new System.Drawing.Size(100, 19);
            this.toolStripStatusLabel1.Text = "Received Packets:";
            // 
            // receivedPacketsValue
            // 
            this.receivedPacketsValue.BorderSides = System.Windows.Forms.ToolStripStatusLabelBorderSides.Right;
            this.receivedPacketsValue.BorderStyle = System.Windows.Forms.Border3DStyle.Etched;
            this.receivedPacketsValue.Name = "receivedPacketsValue";
            this.receivedPacketsValue.Size = new System.Drawing.Size(17, 19);
            this.receivedPacketsValue.Text = "0";
            // 
            // activeConnectionsLabel
            // 
            this.activeConnectionsLabel.BorderStyle = System.Windows.Forms.Border3DStyle.Etched;
            this.activeConnectionsLabel.Name = "activeConnectionsLabel";
            this.activeConnectionsLabel.Size = new System.Drawing.Size(113, 19);
            this.activeConnectionsLabel.Text = "Active Connections:";
            // 
            // activeConnectionsValueLabel
            // 
            this.activeConnectionsValueLabel.BorderSides = System.Windows.Forms.ToolStripStatusLabelBorderSides.Right;
            this.activeConnectionsValueLabel.BorderStyle = System.Windows.Forms.Border3DStyle.Etched;
            this.activeConnectionsValueLabel.Name = "activeConnectionsValueLabel";
            this.activeConnectionsValueLabel.Size = new System.Drawing.Size(17, 19);
            this.activeConnectionsValueLabel.Text = "0";
            // 
            // rocketTabControl
            // 
            this.rocketTabControl.Controls.Add(this.rocket1Tab);
            this.rocketTabControl.Controls.Add(this.newConnectionTab);
            this.rocketTabControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.rocketTabControl.Location = new System.Drawing.Point(0, 0);
            this.rocketTabControl.Name = "rocketTabControl";
            this.rocketTabControl.SelectedIndex = 0;
            this.rocketTabControl.Size = new System.Drawing.Size(1087, 522);
            this.rocketTabControl.TabIndex = 1;
            // 
            // rocket1Tab
            // 
            this.rocket1Tab.Controls.Add(this.tableLayoutPanel1);
            this.rocket1Tab.Location = new System.Drawing.Point(4, 22);
            this.rocket1Tab.Name = "rocket1Tab";
            this.rocket1Tab.Padding = new System.Windows.Forms.Padding(3);
            this.rocket1Tab.Size = new System.Drawing.Size(1079, 496);
            this.rocket1Tab.TabIndex = 0;
            this.rocket1Tab.Text = "(Rocket 1)";
            this.rocket1Tab.UseVisualStyleBackColor = true;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel2, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.telemetryDataGroupBox1, 0, 1);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 2;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(1073, 490);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.AutoSize = true;
            this.tableLayoutPanel2.ColumnCount = 2;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.Controls.Add(this.metLabel1, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.radioLabel1, 1, 0);
            this.tableLayoutPanel2.Controls.Add(this.flightStatusLabel1, 0, 1);
            this.tableLayoutPanel2.Controls.Add(this.stopRecordingButton1, 1, 1);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 2;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(565, 92);
            this.tableLayoutPanel2.TabIndex = 0;
            // 
            // metLabel1
            // 
            this.metLabel1.AutoSize = true;
            this.metLabel1.Controls.Add(this.label1);
            this.metLabel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.metLabel1.ForeColor = System.Drawing.SystemColors.ControlText;
            this.metLabel1.Location = new System.Drawing.Point(3, 3);
            this.metLabel1.Name = "metLabel1";
            this.metLabel1.Size = new System.Drawing.Size(213, 57);
            this.metLabel1.TabIndex = 0;
            this.metLabel1.TabStop = false;
            this.metLabel1.Text = "Mission Elapsed Time";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Black;
            this.label1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label1.Font = new System.Drawing.Font("Consolas", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.label1.Location = new System.Drawing.Point(3, 16);
            this.label1.Name = "label1";
            this.label1.Padding = new System.Windows.Forms.Padding(0, 2, 0, 2);
            this.label1.Size = new System.Drawing.Size(207, 38);
            this.label1.TabIndex = 0;
            this.label1.Text = "+00:00:00.0s";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // radioLabel1
            // 
            this.radioLabel1.AutoSize = true;
            this.radioLabel1.Controls.Add(this.tableLayoutPanel3);
            this.radioLabel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radioLabel1.Location = new System.Drawing.Point(222, 3);
            this.radioLabel1.Name = "radioLabel1";
            this.radioLabel1.Size = new System.Drawing.Size(340, 57);
            this.radioLabel1.TabIndex = 2;
            this.radioLabel1.TabStop = false;
            this.radioLabel1.Text = "Radio";
            // 
            // tableLayoutPanel3
            // 
            this.tableLayoutPanel3.ColumnCount = 6;
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel3.Controls.Add(this.label44, 3, 0);
            this.tableLayoutPanel3.Controls.Add(this.label43, 2, 0);
            this.tableLayoutPanel3.Controls.Add(this.label42, 1, 0);
            this.tableLayoutPanel3.Controls.Add(this.label41, 0, 0);
            this.tableLayoutPanel3.Controls.Add(this.label48, 4, 0);
            this.tableLayoutPanel3.Controls.Add(this.label49, 5, 0);
            this.tableLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel3.Location = new System.Drawing.Point(3, 16);
            this.tableLayoutPanel3.Name = "tableLayoutPanel3";
            this.tableLayoutPanel3.RowCount = 1;
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel3.Size = new System.Drawing.Size(334, 38);
            this.tableLayoutPanel3.TabIndex = 0;
            // 
            // label44
            // 
            this.label44.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.label44.AutoEllipsis = true;
            this.label44.AutoSize = true;
            this.label44.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label44.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label44.Location = new System.Drawing.Point(122, 0);
            this.label44.Name = "label44";
            this.label44.Size = new System.Drawing.Size(15, 38);
            this.label44.TabIndex = 5;
            this.label44.Text = "0";
            this.label44.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label43
            // 
            this.label43.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.label43.AutoEllipsis = true;
            this.label43.AutoSize = true;
            this.label43.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label43.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label43.Location = new System.Drawing.Point(73, 0);
            this.label43.Name = "label43";
            this.label43.Size = new System.Drawing.Size(43, 38);
            this.label43.TabIndex = 4;
            this.label43.Text = "Recv:";
            this.label43.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label42
            // 
            this.label42.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.label42.AutoEllipsis = true;
            this.label42.AutoSize = true;
            this.label42.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label42.ForeColor = System.Drawing.SystemColors.ControlDark;
            this.label42.Location = new System.Drawing.Point(36, 0);
            this.label42.Name = "label42";
            this.label42.Size = new System.Drawing.Size(31, 38);
            this.label42.TabIndex = 3;
            this.label42.Text = "N/A";
            this.label42.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label41
            // 
            this.label41.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.label41.AutoEllipsis = true;
            this.label41.AutoSize = true;
            this.label41.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label41.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label41.Location = new System.Drawing.Point(3, 0);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(27, 38);
            this.label41.TabIndex = 2;
            this.label41.Text = "Ch:";
            this.label41.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label48
            // 
            this.label48.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.label48.AutoEllipsis = true;
            this.label48.AutoSize = true;
            this.label48.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label48.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label48.Location = new System.Drawing.Point(143, 0);
            this.label48.Name = "label48";
            this.label48.Size = new System.Drawing.Size(41, 38);
            this.label48.TabIndex = 6;
            this.label48.Text = "Drop:";
            this.label48.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label49
            // 
            this.label49.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.label49.AutoEllipsis = true;
            this.label49.AutoSize = true;
            this.label49.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label49.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label49.Location = new System.Drawing.Point(190, 0);
            this.label49.Name = "label49";
            this.label49.Size = new System.Drawing.Size(15, 38);
            this.label49.TabIndex = 7;
            this.label49.Text = "0";
            this.label49.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // flightStatusLabel1
            // 
            this.flightStatusLabel1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.flightStatusLabel1.AutoEllipsis = true;
            this.flightStatusLabel1.AutoSize = true;
            this.flightStatusLabel1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.flightStatusLabel1.ForeColor = System.Drawing.SystemColors.ControlDark;
            this.flightStatusLabel1.Location = new System.Drawing.Point(3, 63);
            this.flightStatusLabel1.Name = "flightStatusLabel1";
            this.flightStatusLabel1.Size = new System.Drawing.Size(126, 29);
            this.flightStatusLabel1.TabIndex = 1;
            this.flightStatusLabel1.Text = "(Not Connected)";
            this.flightStatusLabel1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // stopRecordingButton1
            // 
            this.stopRecordingButton1.AutoSize = true;
            this.stopRecordingButton1.Enabled = false;
            this.stopRecordingButton1.Location = new System.Drawing.Point(222, 66);
            this.stopRecordingButton1.Name = "stopRecordingButton1";
            this.stopRecordingButton1.Size = new System.Drawing.Size(93, 23);
            this.stopRecordingButton1.TabIndex = 3;
            this.stopRecordingButton1.Text = "Stop Recording";
            this.stopRecordingButton1.UseVisualStyleBackColor = true;
            // 
            // telemetryDataGroupBox1
            // 
            this.telemetryDataGroupBox1.AutoSize = true;
            this.telemetryDataGroupBox1.Controls.Add(this.tableLayoutPanel4);
            this.telemetryDataGroupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.telemetryDataGroupBox1.Location = new System.Drawing.Point(3, 101);
            this.telemetryDataGroupBox1.MinimumSize = new System.Drawing.Size(565, 0);
            this.telemetryDataGroupBox1.Name = "telemetryDataGroupBox1";
            this.telemetryDataGroupBox1.Size = new System.Drawing.Size(565, 386);
            this.telemetryDataGroupBox1.TabIndex = 1;
            this.telemetryDataGroupBox1.TabStop = false;
            this.telemetryDataGroupBox1.Text = "Telemetry Data";
            // 
            // tableLayoutPanel4
            // 
            this.tableLayoutPanel4.ColumnCount = 2;
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 19.46565F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 80.53435F));
            this.tableLayoutPanel4.Controls.Add(this.flowLayoutPanel3, 1, 2);
            this.tableLayoutPanel4.Controls.Add(this.label2, 0, 0);
            this.tableLayoutPanel4.Controls.Add(this.label3, 0, 2);
            this.tableLayoutPanel4.Controls.Add(this.label4, 0, 1);
            this.tableLayoutPanel4.Controls.Add(this.label5, 0, 4);
            this.tableLayoutPanel4.Controls.Add(this.label6, 0, 5);
            this.tableLayoutPanel4.Controls.Add(this.label7, 0, 6);
            this.tableLayoutPanel4.Controls.Add(this.flowLayoutPanel1, 1, 0);
            this.tableLayoutPanel4.Controls.Add(this.flowLayoutPanel2, 1, 1);
            this.tableLayoutPanel4.Controls.Add(this.flowLayoutPanel4, 1, 4);
            this.tableLayoutPanel4.Controls.Add(this.flowLayoutPanel5, 1, 5);
            this.tableLayoutPanel4.Controls.Add(this.flowLayoutPanel6, 1, 6);
            this.tableLayoutPanel4.Controls.Add(this.label35, 0, 7);
            this.tableLayoutPanel4.Controls.Add(this.flowLayoutPanel7, 1, 7);
            this.tableLayoutPanel4.Controls.Add(this.eventLog1, 1, 9);
            this.tableLayoutPanel4.Controls.Add(this.label50, 0, 9);
            this.tableLayoutPanel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel4.Location = new System.Drawing.Point(3, 16);
            this.tableLayoutPanel4.Name = "tableLayoutPanel4";
            this.tableLayoutPanel4.RowCount = 10;
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel4.Size = new System.Drawing.Size(559, 367);
            this.tableLayoutPanel4.TabIndex = 0;
            // 
            // flowLayoutPanel3
            // 
            this.flowLayoutPanel3.AutoSize = true;
            this.flowLayoutPanel3.Controls.Add(this.label16);
            this.flowLayoutPanel3.Controls.Add(this.textBox6);
            this.flowLayoutPanel3.Controls.Add(this.label17);
            this.flowLayoutPanel3.Controls.Add(this.textBox7);
            this.flowLayoutPanel3.Controls.Add(this.label18);
            this.flowLayoutPanel3.Controls.Add(this.textBox8);
            this.flowLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flowLayoutPanel3.Location = new System.Drawing.Point(111, 67);
            this.flowLayoutPanel3.Name = "flowLayoutPanel3";
            this.flowLayoutPanel3.Size = new System.Drawing.Size(445, 26);
            this.flowLayoutPanel3.TabIndex = 8;
            // 
            // label16
            // 
            this.label16.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(3, 0);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(23, 26);
            this.label16.TabIndex = 5;
            this.label16.Text = "Ch:";
            this.label16.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // textBox6
            // 
            this.textBox6.Location = new System.Drawing.Point(32, 3);
            this.textBox6.Name = "textBox6";
            this.textBox6.ReadOnly = true;
            this.textBox6.Size = new System.Drawing.Size(53, 20);
            this.textBox6.TabIndex = 2;
            // 
            // label17
            // 
            this.label17.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(91, 0);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(26, 26);
            this.label17.TabIndex = 6;
            this.label17.Text = "Snt:";
            this.label17.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // textBox7
            // 
            this.textBox7.Location = new System.Drawing.Point(123, 3);
            this.textBox7.Name = "textBox7";
            this.textBox7.ReadOnly = true;
            this.textBox7.Size = new System.Drawing.Size(53, 20);
            this.textBox7.TabIndex = 3;
            // 
            // label18
            // 
            this.label18.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(182, 0);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(42, 26);
            this.label18.TabIndex = 7;
            this.label18.Text = "ChkErr:";
            this.label18.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // textBox8
            // 
            this.textBox8.Location = new System.Drawing.Point(230, 3);
            this.textBox8.Name = "textBox8";
            this.textBox8.ReadOnly = true;
            this.textBox8.Size = new System.Drawing.Size(53, 20);
            this.textBox8.TabIndex = 4;
            // 
            // label2
            // 
            this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(3, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(88, 32);
            this.label2.TabIndex = 0;
            this.label2.Text = "GPS Coordinates";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label3
            // 
            this.label3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(3, 64);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(56, 32);
            this.label3.TabIndex = 1;
            this.label3.Text = "GPS Stats";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label4
            // 
            this.label4.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(3, 32);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(65, 32);
            this.label4.TabIndex = 2;
            this.label4.Text = "GPS Course";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label5
            // 
            this.label5.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(3, 116);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(47, 32);
            this.label5.TabIndex = 3;
            this.label5.Text = "Altimeter";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label6
            // 
            this.label6.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(3, 148);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(44, 32);
            this.label6.TabIndex = 4;
            this.label6.Text = "Velocity";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label7
            // 
            this.label7.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(3, 180);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(66, 32);
            this.label7.TabIndex = 5;
            this.label7.Text = "Acceleration";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.AutoSize = true;
            this.flowLayoutPanel1.Controls.Add(this.label8);
            this.flowLayoutPanel1.Controls.Add(this.latitude1);
            this.flowLayoutPanel1.Controls.Add(this.label32);
            this.flowLayoutPanel1.Controls.Add(this.label9);
            this.flowLayoutPanel1.Controls.Add(this.textBox2);
            this.flowLayoutPanel1.Controls.Add(this.label33);
            this.flowLayoutPanel1.Controls.Add(this.label13);
            this.flowLayoutPanel1.Controls.Add(this.textBox3);
            this.flowLayoutPanel1.Controls.Add(this.label14);
            this.flowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flowLayoutPanel1.Location = new System.Drawing.Point(111, 3);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(445, 26);
            this.flowLayoutPanel1.TabIndex = 6;
            // 
            // label8
            // 
            this.label8.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(3, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(25, 26);
            this.label8.TabIndex = 0;
            this.label8.Text = "Lat:";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // latitude1
            // 
            this.latitude1.Location = new System.Drawing.Point(34, 3);
            this.latitude1.Name = "latitude1";
            this.latitude1.ReadOnly = true;
            this.latitude1.Size = new System.Drawing.Size(100, 20);
            this.latitude1.TabIndex = 1;
            // 
            // label32
            // 
            this.label32.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.label32.AutoSize = true;
            this.label32.Location = new System.Drawing.Point(140, 0);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(11, 26);
            this.label32.TabIndex = 12;
            this.label32.Text = "°";
            this.label32.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label9
            // 
            this.label9.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(157, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(28, 26);
            this.label9.TabIndex = 2;
            this.label9.Text = "Lon:";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // textBox2
            // 
            this.textBox2.Location = new System.Drawing.Point(191, 3);
            this.textBox2.Name = "textBox2";
            this.textBox2.ReadOnly = true;
            this.textBox2.Size = new System.Drawing.Size(100, 20);
            this.textBox2.TabIndex = 3;
            // 
            // label33
            // 
            this.label33.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.label33.AutoSize = true;
            this.label33.Location = new System.Drawing.Point(297, 0);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(11, 26);
            this.label33.TabIndex = 13;
            this.label33.Text = "°";
            this.label33.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label13
            // 
            this.label13.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(314, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(22, 26);
            this.label13.TabIndex = 4;
            this.label13.Text = "Alt:";
            this.label13.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // textBox3
            // 
            this.textBox3.Location = new System.Drawing.Point(342, 3);
            this.textBox3.Name = "textBox3";
            this.textBox3.ReadOnly = true;
            this.textBox3.Size = new System.Drawing.Size(66, 20);
            this.textBox3.TabIndex = 14;
            // 
            // label14
            // 
            this.label14.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(414, 0);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(15, 26);
            this.label14.TabIndex = 6;
            this.label14.Text = "m";
            this.label14.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // flowLayoutPanel2
            // 
            this.flowLayoutPanel2.AutoSize = true;
            this.flowLayoutPanel2.Controls.Add(this.label10);
            this.flowLayoutPanel2.Controls.Add(this.textBox4);
            this.flowLayoutPanel2.Controls.Add(this.label34);
            this.flowLayoutPanel2.Controls.Add(this.label15);
            this.flowLayoutPanel2.Controls.Add(this.textBox5);
            this.flowLayoutPanel2.Controls.Add(this.label11);
            this.flowLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flowLayoutPanel2.Location = new System.Drawing.Point(111, 35);
            this.flowLayoutPanel2.Name = "flowLayoutPanel2";
            this.flowLayoutPanel2.Size = new System.Drawing.Size(445, 26);
            this.flowLayoutPanel2.TabIndex = 7;
            // 
            // label10
            // 
            this.label10.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(3, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(43, 26);
            this.label10.TabIndex = 0;
            this.label10.Text = "Course:";
            this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // textBox4
            // 
            this.textBox4.Location = new System.Drawing.Point(52, 3);
            this.textBox4.Name = "textBox4";
            this.textBox4.ReadOnly = true;
            this.textBox4.Size = new System.Drawing.Size(53, 20);
            this.textBox4.TabIndex = 1;
            // 
            // label34
            // 
            this.label34.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.label34.AutoSize = true;
            this.label34.Location = new System.Drawing.Point(111, 0);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(11, 26);
            this.label34.TabIndex = 14;
            this.label34.Text = "°";
            this.label34.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label15
            // 
            this.label15.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(128, 0);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(41, 26);
            this.label15.TabIndex = 2;
            this.label15.Text = "Speed:";
            this.label15.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // textBox5
            // 
            this.textBox5.Location = new System.Drawing.Point(175, 3);
            this.textBox5.Name = "textBox5";
            this.textBox5.ReadOnly = true;
            this.textBox5.Size = new System.Drawing.Size(53, 20);
            this.textBox5.TabIndex = 3;
            // 
            // label11
            // 
            this.label11.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(234, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(25, 26);
            this.label11.TabIndex = 7;
            this.label11.Text = "m/s";
            this.label11.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // flowLayoutPanel4
            // 
            this.flowLayoutPanel4.AutoSize = true;
            this.flowLayoutPanel4.Controls.Add(this.label19);
            this.flowLayoutPanel4.Controls.Add(this.textBox9);
            this.flowLayoutPanel4.Controls.Add(this.label20);
            this.flowLayoutPanel4.Controls.Add(this.label12);
            this.flowLayoutPanel4.Controls.Add(this.textBox10);
            this.flowLayoutPanel4.Controls.Add(this.label21);
            this.flowLayoutPanel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flowLayoutPanel4.Location = new System.Drawing.Point(111, 119);
            this.flowLayoutPanel4.Name = "flowLayoutPanel4";
            this.flowLayoutPanel4.Size = new System.Drawing.Size(445, 26);
            this.flowLayoutPanel4.TabIndex = 9;
            // 
            // label19
            // 
            this.label19.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(3, 0);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(30, 26);
            this.label19.TabIndex = 6;
            this.label19.Text = "ASL:";
            this.label19.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // textBox9
            // 
            this.textBox9.Location = new System.Drawing.Point(39, 3);
            this.textBox9.Name = "textBox9";
            this.textBox9.ReadOnly = true;
            this.textBox9.Size = new System.Drawing.Size(66, 20);
            this.textBox9.TabIndex = 7;
            // 
            // label20
            // 
            this.label20.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(111, 0);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(15, 26);
            this.label20.TabIndex = 10;
            this.label20.Text = "m";
            this.label20.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label12
            // 
            this.label12.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(132, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(31, 26);
            this.label12.TabIndex = 8;
            this.label12.Text = "AGL:";
            this.label12.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // textBox10
            // 
            this.textBox10.Location = new System.Drawing.Point(169, 3);
            this.textBox10.Name = "textBox10";
            this.textBox10.ReadOnly = true;
            this.textBox10.Size = new System.Drawing.Size(66, 20);
            this.textBox10.TabIndex = 12;
            // 
            // label21
            // 
            this.label21.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(241, 0);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(15, 26);
            this.label21.TabIndex = 11;
            this.label21.Text = "m";
            this.label21.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // flowLayoutPanel5
            // 
            this.flowLayoutPanel5.AutoSize = true;
            this.flowLayoutPanel5.Controls.Add(this.label22);
            this.flowLayoutPanel5.Controls.Add(this.textBox11);
            this.flowLayoutPanel5.Controls.Add(this.label23);
            this.flowLayoutPanel5.Controls.Add(this.textBox12);
            this.flowLayoutPanel5.Controls.Add(this.label24);
            this.flowLayoutPanel5.Controls.Add(this.textBox13);
            this.flowLayoutPanel5.Controls.Add(this.label25);
            this.flowLayoutPanel5.Controls.Add(this.label26);
            this.flowLayoutPanel5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flowLayoutPanel5.Location = new System.Drawing.Point(111, 151);
            this.flowLayoutPanel5.Name = "flowLayoutPanel5";
            this.flowLayoutPanel5.Size = new System.Drawing.Size(445, 26);
            this.flowLayoutPanel5.TabIndex = 11;
            // 
            // label22
            // 
            this.label22.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.Location = new System.Drawing.Point(3, 0);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(13, 26);
            this.label22.TabIndex = 10;
            this.label22.Text = "{";
            this.label22.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // textBox11
            // 
            this.textBox11.Location = new System.Drawing.Point(22, 3);
            this.textBox11.Name = "textBox11";
            this.textBox11.ReadOnly = true;
            this.textBox11.Size = new System.Drawing.Size(53, 20);
            this.textBox11.TabIndex = 11;
            // 
            // label23
            // 
            this.label23.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label23.Location = new System.Drawing.Point(81, 0);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(12, 26);
            this.label23.TabIndex = 12;
            this.label23.Text = ",";
            this.label23.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // textBox12
            // 
            this.textBox12.Location = new System.Drawing.Point(99, 3);
            this.textBox12.Name = "textBox12";
            this.textBox12.ReadOnly = true;
            this.textBox12.Size = new System.Drawing.Size(53, 20);
            this.textBox12.TabIndex = 15;
            // 
            // label24
            // 
            this.label24.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.label24.AutoSize = true;
            this.label24.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label24.Location = new System.Drawing.Point(158, 0);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(12, 26);
            this.label24.TabIndex = 13;
            this.label24.Text = ",";
            this.label24.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // textBox13
            // 
            this.textBox13.Location = new System.Drawing.Point(176, 3);
            this.textBox13.Name = "textBox13";
            this.textBox13.ReadOnly = true;
            this.textBox13.Size = new System.Drawing.Size(53, 20);
            this.textBox13.TabIndex = 16;
            // 
            // label25
            // 
            this.label25.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.label25.AutoSize = true;
            this.label25.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label25.Location = new System.Drawing.Point(235, 0);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(13, 26);
            this.label25.TabIndex = 14;
            this.label25.Text = "}";
            this.label25.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label26
            // 
            this.label26.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.label26.AutoSize = true;
            this.label26.Location = new System.Drawing.Point(254, 0);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(25, 26);
            this.label26.TabIndex = 17;
            this.label26.Text = "m/s";
            this.label26.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // flowLayoutPanel6
            // 
            this.flowLayoutPanel6.AutoSize = true;
            this.flowLayoutPanel6.Controls.Add(this.label27);
            this.flowLayoutPanel6.Controls.Add(this.textBox14);
            this.flowLayoutPanel6.Controls.Add(this.label28);
            this.flowLayoutPanel6.Controls.Add(this.textBox15);
            this.flowLayoutPanel6.Controls.Add(this.label29);
            this.flowLayoutPanel6.Controls.Add(this.textBox16);
            this.flowLayoutPanel6.Controls.Add(this.label30);
            this.flowLayoutPanel6.Controls.Add(this.label31);
            this.flowLayoutPanel6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flowLayoutPanel6.Location = new System.Drawing.Point(111, 183);
            this.flowLayoutPanel6.Name = "flowLayoutPanel6";
            this.flowLayoutPanel6.Size = new System.Drawing.Size(445, 26);
            this.flowLayoutPanel6.TabIndex = 12;
            // 
            // label27
            // 
            this.label27.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.label27.AutoSize = true;
            this.label27.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label27.Location = new System.Drawing.Point(3, 0);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(13, 26);
            this.label27.TabIndex = 11;
            this.label27.Text = "{";
            this.label27.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // textBox14
            // 
            this.textBox14.Location = new System.Drawing.Point(22, 3);
            this.textBox14.Name = "textBox14";
            this.textBox14.ReadOnly = true;
            this.textBox14.Size = new System.Drawing.Size(53, 20);
            this.textBox14.TabIndex = 12;
            // 
            // label28
            // 
            this.label28.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.label28.AutoSize = true;
            this.label28.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label28.Location = new System.Drawing.Point(81, 0);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(12, 26);
            this.label28.TabIndex = 13;
            this.label28.Text = ",";
            this.label28.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // textBox15
            // 
            this.textBox15.Location = new System.Drawing.Point(99, 3);
            this.textBox15.Name = "textBox15";
            this.textBox15.ReadOnly = true;
            this.textBox15.Size = new System.Drawing.Size(53, 20);
            this.textBox15.TabIndex = 16;
            // 
            // label29
            // 
            this.label29.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.label29.AutoSize = true;
            this.label29.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label29.Location = new System.Drawing.Point(158, 0);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(12, 26);
            this.label29.TabIndex = 17;
            this.label29.Text = ",";
            this.label29.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // textBox16
            // 
            this.textBox16.Location = new System.Drawing.Point(176, 3);
            this.textBox16.Name = "textBox16";
            this.textBox16.ReadOnly = true;
            this.textBox16.Size = new System.Drawing.Size(53, 20);
            this.textBox16.TabIndex = 18;
            // 
            // label30
            // 
            this.label30.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.label30.AutoSize = true;
            this.label30.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label30.Location = new System.Drawing.Point(235, 0);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(13, 26);
            this.label30.TabIndex = 19;
            this.label30.Text = "}";
            this.label30.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label31
            // 
            this.label31.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.label31.AutoSize = true;
            this.label31.Location = new System.Drawing.Point(254, 0);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(28, 26);
            this.label31.TabIndex = 20;
            this.label31.Text = "m/s²";
            this.label31.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label35
            // 
            this.label35.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.label35.AutoSize = true;
            this.label35.Location = new System.Drawing.Point(3, 212);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(83, 32);
            this.label35.TabIndex = 13;
            this.label35.Text = "Angular Velocity";
            this.label35.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // flowLayoutPanel7
            // 
            this.flowLayoutPanel7.AutoSize = true;
            this.flowLayoutPanel7.Controls.Add(this.label36);
            this.flowLayoutPanel7.Controls.Add(this.textBox17);
            this.flowLayoutPanel7.Controls.Add(this.label37);
            this.flowLayoutPanel7.Controls.Add(this.textBox18);
            this.flowLayoutPanel7.Controls.Add(this.label38);
            this.flowLayoutPanel7.Controls.Add(this.textBox19);
            this.flowLayoutPanel7.Controls.Add(this.label39);
            this.flowLayoutPanel7.Controls.Add(this.label40);
            this.flowLayoutPanel7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flowLayoutPanel7.Location = new System.Drawing.Point(111, 215);
            this.flowLayoutPanel7.Name = "flowLayoutPanel7";
            this.flowLayoutPanel7.Size = new System.Drawing.Size(445, 26);
            this.flowLayoutPanel7.TabIndex = 14;
            // 
            // label36
            // 
            this.label36.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.label36.AutoSize = true;
            this.label36.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label36.Location = new System.Drawing.Point(3, 0);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(13, 26);
            this.label36.TabIndex = 12;
            this.label36.Text = "{";
            this.label36.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // textBox17
            // 
            this.textBox17.Location = new System.Drawing.Point(22, 3);
            this.textBox17.Name = "textBox17";
            this.textBox17.ReadOnly = true;
            this.textBox17.Size = new System.Drawing.Size(53, 20);
            this.textBox17.TabIndex = 22;
            // 
            // label37
            // 
            this.label37.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.label37.AutoSize = true;
            this.label37.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label37.Location = new System.Drawing.Point(81, 0);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(12, 26);
            this.label37.TabIndex = 13;
            this.label37.Text = ",";
            this.label37.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // textBox18
            // 
            this.textBox18.Location = new System.Drawing.Point(99, 3);
            this.textBox18.Name = "textBox18";
            this.textBox18.ReadOnly = true;
            this.textBox18.Size = new System.Drawing.Size(53, 20);
            this.textBox18.TabIndex = 23;
            // 
            // label38
            // 
            this.label38.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.label38.AutoSize = true;
            this.label38.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label38.Location = new System.Drawing.Point(158, 0);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(12, 26);
            this.label38.TabIndex = 14;
            this.label38.Text = ",";
            this.label38.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // textBox19
            // 
            this.textBox19.Location = new System.Drawing.Point(176, 3);
            this.textBox19.Name = "textBox19";
            this.textBox19.ReadOnly = true;
            this.textBox19.Size = new System.Drawing.Size(53, 20);
            this.textBox19.TabIndex = 24;
            // 
            // label39
            // 
            this.label39.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.label39.AutoSize = true;
            this.label39.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label39.Location = new System.Drawing.Point(235, 0);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(13, 26);
            this.label39.TabIndex = 15;
            this.label39.Text = "}";
            this.label39.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label40
            // 
            this.label40.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.label40.AutoSize = true;
            this.label40.Location = new System.Drawing.Point(254, 0);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(21, 26);
            this.label40.TabIndex = 21;
            this.label40.Text = "°/s";
            this.label40.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // eventLog1
            // 
            this.eventLog1.BackColor = System.Drawing.SystemColors.Window;
            this.eventLog1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.eventLog1.Font = new System.Drawing.Font("Consolas", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.eventLog1.Location = new System.Drawing.Point(111, 267);
            this.eventLog1.Multiline = true;
            this.eventLog1.Name = "eventLog1";
            this.eventLog1.ReadOnly = true;
            this.eventLog1.Size = new System.Drawing.Size(445, 97);
            this.eventLog1.TabIndex = 15;
            // 
            // label50
            // 
            this.label50.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.label50.AutoSize = true;
            this.label50.Location = new System.Drawing.Point(3, 264);
            this.label50.Name = "label50";
            this.label50.Size = new System.Drawing.Size(81, 103);
            this.label50.TabIndex = 16;
            this.label50.Text = "FlightEvent Log";
            this.label50.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // newConnectionTab
            // 
            this.newConnectionTab.Controls.Add(this.tableLayoutPanel5);
            this.newConnectionTab.Location = new System.Drawing.Point(4, 22);
            this.newConnectionTab.Name = "newConnectionTab";
            this.newConnectionTab.Size = new System.Drawing.Size(1079, 496);
            this.newConnectionTab.TabIndex = 1;
            this.newConnectionTab.Text = "New Flight...";
            this.newConnectionTab.UseVisualStyleBackColor = true;
            // 
            // tableLayoutPanel5
            // 
            this.tableLayoutPanel5.ColumnCount = 2;
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel5.Controls.Add(this.label47, 0, 0);
            this.tableLayoutPanel5.Controls.Add(this.label45, 0, 1);
            this.tableLayoutPanel5.Controls.Add(this.label46, 0, 2);
            this.tableLayoutPanel5.Controls.Add(this.textBox20, 1, 1);
            this.tableLayoutPanel5.Controls.Add(this.newFlightChannelSelect, 1, 2);
            this.tableLayoutPanel5.Controls.Add(this.flowLayoutPanel8, 1, 0);
            this.tableLayoutPanel5.Controls.Add(this.button1, 1, 3);
            this.tableLayoutPanel5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel5.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel5.Name = "tableLayoutPanel5";
            this.tableLayoutPanel5.RowCount = 4;
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel5.Size = new System.Drawing.Size(1079, 496);
            this.tableLayoutPanel5.TabIndex = 0;
            // 
            // label47
            // 
            this.label47.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.label47.AutoSize = true;
            this.label47.Location = new System.Drawing.Point(3, 0);
            this.label47.Name = "label47";
            this.label47.Size = new System.Drawing.Size(31, 29);
            this.label47.TabIndex = 4;
            this.label47.Text = "Type";
            this.label47.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label45
            // 
            this.label45.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.label45.AutoSize = true;
            this.label45.Location = new System.Drawing.Point(3, 29);
            this.label45.Name = "label45";
            this.label45.Size = new System.Drawing.Size(73, 26);
            this.label45.TabIndex = 0;
            this.label45.Text = "Rocket Name";
            this.label45.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label46
            // 
            this.label46.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.label46.AutoSize = true;
            this.label46.Location = new System.Drawing.Point(3, 55);
            this.label46.Name = "label46";
            this.label46.Size = new System.Drawing.Size(77, 26);
            this.label46.TabIndex = 1;
            this.label46.Text = "Radio Channel";
            this.label46.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // textBox20
            // 
            this.textBox20.Location = new System.Drawing.Point(86, 32);
            this.textBox20.Name = "textBox20";
            this.textBox20.Size = new System.Drawing.Size(213, 20);
            this.textBox20.TabIndex = 3;
            // 
            // newFlightChannelSelect
            // 
            this.newFlightChannelSelect.Location = new System.Drawing.Point(86, 58);
            this.newFlightChannelSelect.Maximum = new decimal(new int[] {
            15,
            0,
            0,
            0});
            this.newFlightChannelSelect.Name = "newFlightChannelSelect";
            this.newFlightChannelSelect.Size = new System.Drawing.Size(42, 20);
            this.newFlightChannelSelect.TabIndex = 5;
            // 
            // flowLayoutPanel8
            // 
            this.flowLayoutPanel8.AutoSize = true;
            this.flowLayoutPanel8.Controls.Add(this.newFlightTypeStandardRadioButton);
            this.flowLayoutPanel8.Controls.Add(this.newFlightTypeLegacyRadioButton);
            this.flowLayoutPanel8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flowLayoutPanel8.Location = new System.Drawing.Point(86, 3);
            this.flowLayoutPanel8.Name = "flowLayoutPanel8";
            this.flowLayoutPanel8.Size = new System.Drawing.Size(990, 23);
            this.flowLayoutPanel8.TabIndex = 6;
            // 
            // newFlightTypeStandardRadioButton
            // 
            this.newFlightTypeStandardRadioButton.AutoSize = true;
            this.newFlightTypeStandardRadioButton.Checked = true;
            this.newFlightTypeStandardRadioButton.Location = new System.Drawing.Point(3, 3);
            this.newFlightTypeStandardRadioButton.Name = "newFlightTypeStandardRadioButton";
            this.newFlightTypeStandardRadioButton.Size = new System.Drawing.Size(108, 17);
            this.newFlightTypeStandardRadioButton.TabIndex = 0;
            this.newFlightTypeStandardRadioButton.TabStop = true;
            this.newFlightTypeStandardRadioButton.Text = "Standard (Finster)";
            this.newFlightTypeStandardRadioButton.UseVisualStyleBackColor = true;
            // 
            // newFlightTypeLegacyRadioButton
            // 
            this.newFlightTypeLegacyRadioButton.AutoSize = true;
            this.newFlightTypeLegacyRadioButton.Location = new System.Drawing.Point(117, 3);
            this.newFlightTypeLegacyRadioButton.Name = "newFlightTypeLegacyRadioButton";
            this.newFlightTypeLegacyRadioButton.Size = new System.Drawing.Size(148, 17);
            this.newFlightTypeLegacyRadioButton.TabIndex = 1;
            this.newFlightTypeLegacyRadioButton.Text = "Legacy (Standalone GPS)";
            this.newFlightTypeLegacyRadioButton.UseVisualStyleBackColor = true;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(86, 84);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 7;
            this.button1.Text = "Create";
            this.button1.UseVisualStyleBackColor = true;
            // 
            // flightBindingSource
            // 
            this.flightBindingSource.DataSource = typeof(FinsterTelemetryProtocol.Flight);
            // 
            // FormProgram
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1087, 546);
            this.Controls.Add(this.rocketTabControl);
            this.Controls.Add(this.statusStrip1);
            this.Name = "FormProgram";
            this.Text = "Finster Telemetry";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.rocketTabControl.ResumeLayout(false);
            this.rocket1Tab.ResumeLayout(false);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.tableLayoutPanel2.ResumeLayout(false);
            this.tableLayoutPanel2.PerformLayout();
            this.metLabel1.ResumeLayout(false);
            this.metLabel1.PerformLayout();
            this.radioLabel1.ResumeLayout(false);
            this.tableLayoutPanel3.ResumeLayout(false);
            this.tableLayoutPanel3.PerformLayout();
            this.telemetryDataGroupBox1.ResumeLayout(false);
            this.tableLayoutPanel4.ResumeLayout(false);
            this.tableLayoutPanel4.PerformLayout();
            this.flowLayoutPanel3.ResumeLayout(false);
            this.flowLayoutPanel3.PerformLayout();
            this.flowLayoutPanel1.ResumeLayout(false);
            this.flowLayoutPanel1.PerformLayout();
            this.flowLayoutPanel2.ResumeLayout(false);
            this.flowLayoutPanel2.PerformLayout();
            this.flowLayoutPanel4.ResumeLayout(false);
            this.flowLayoutPanel4.PerformLayout();
            this.flowLayoutPanel5.ResumeLayout(false);
            this.flowLayoutPanel5.PerformLayout();
            this.flowLayoutPanel6.ResumeLayout(false);
            this.flowLayoutPanel6.PerformLayout();
            this.flowLayoutPanel7.ResumeLayout(false);
            this.flowLayoutPanel7.PerformLayout();
            this.newConnectionTab.ResumeLayout(false);
            this.tableLayoutPanel5.ResumeLayout(false);
            this.tableLayoutPanel5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.newFlightChannelSelect)).EndInit();
            this.flowLayoutPanel8.ResumeLayout(false);
            this.flowLayoutPanel8.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.flightBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel connectionStatusLabel;
        private System.Windows.Forms.ToolStripStatusLabel activeConnectionsLabel;
        private System.Windows.Forms.ToolStripStatusLabel activeConnectionsValueLabel;
        private System.Windows.Forms.TabControl rocketTabControl;
        private System.Windows.Forms.TabPage rocket1Tab;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.GroupBox metLabel1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label flightStatusLabel1;
        private System.Windows.Forms.GroupBox radioLabel1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel3;
        private System.Windows.Forms.Button stopRecordingButton1;
        private System.Windows.Forms.TabPage newConnectionTab;
        private System.Windows.Forms.GroupBox telemetryDataGroupBox1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel4;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel2;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel3;
        private System.Windows.Forms.TextBox latitude1;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel4;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox textBox4;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.TextBox textBox6;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.TextBox textBox7;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.TextBox textBox8;
        private System.Windows.Forms.TextBox textBox5;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.TextBox textBox9;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel5;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.TextBox textBox11;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.TextBox textBox12;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.TextBox textBox13;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel6;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.TextBox textBox15;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.TextBox textBox16;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.TextBox textBox14;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel7;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.TextBox textBox17;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.TextBox textBox18;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.TextBox textBox19;
        private System.Windows.Forms.Label label39;
        private System.Windows.Forms.Label label40;
        private System.Windows.Forms.TextBox textBox3;
        private System.Windows.Forms.TextBox textBox10;
        private System.Windows.Forms.ToolStripStatusLabel droppedPacketsLabel;
        private System.Windows.Forms.ToolStripStatusLabel droppedPacketsValue;
        private System.Windows.Forms.ToolStripStatusLabel connectionActivityBubble;
        private System.Windows.Forms.Label label44;
        private System.Windows.Forms.Label label43;
        private System.Windows.Forms.Label label42;
        private System.Windows.Forms.Label label41;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel5;
        private System.Windows.Forms.Label label45;
        private System.Windows.Forms.Label label46;
        private System.Windows.Forms.Label label47;
        private System.Windows.Forms.TextBox textBox20;
        private System.Windows.Forms.NumericUpDown newFlightChannelSelect;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel8;
        private System.Windows.Forms.RadioButton newFlightTypeStandardRadioButton;
        private System.Windows.Forms.RadioButton newFlightTypeLegacyRadioButton;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label label48;
        private System.Windows.Forms.Label label49;
        private System.Windows.Forms.TextBox eventLog1;
        private System.Windows.Forms.Label label50;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel1;
        private System.Windows.Forms.ToolStripStatusLabel receivedPacketsValue;
        private System.Windows.Forms.BindingSource flightBindingSource;
    }
}

