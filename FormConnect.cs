﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FinsterMissionControl
{
    public partial class FormConnect : Form
    {
        private SerialPort _radio;
        private FormProgram _form1;

        public FormConnect(SerialPort radio, FormProgram form1)
        {
            InitializeComponent();

            _radio = radio;
            _form1 = form1;

            comPortComboBox.Items.Clear();

            foreach (string port in SerialPort.GetPortNames())
            {
                comPortComboBox.Items.Add((object)port);
            }
        }

        private void cancelButton_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void connectButton_Click(object sender, EventArgs e)
        {
            int baud;
            if (int.TryParse((string) baudRateComboBox.SelectedItem, out baud))
            {
                _radio.BaudRate = baud;
            }
            else
            {
                // handle improper baud rate
                MessageBox.Show(@"Improper Baudrate selected.");
                return;
            }

            string com = (string) comPortComboBox.SelectedItem;
            if (com != null && com.StartsWith("COM"))
            {
                _radio.PortName = com;
            }
            else
            {
                // handle incorrect COM port
                MessageBox.Show(string.Format("\"{0}\" is not a valid COM Port.", com ?? "null"));
                return;
            }

            Connect();

            Close();
        }

        public void Connect()
        {
            try
            {
                _radio.Open();
            }
            catch (Exception ex)
            {
                MessageBox.Show(string.Format("An error occured when opening the COM Port: {0}", ex.Message));
                return;
            }

            _form1.Connected();
        }
    }
}
